<?php

namespace Drupal\form_serialization\Encoder;

use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * Adds application/x-www-form-urlencoded support for the Serialization API.
 */
class FormEncoder implements EncoderInterface, DecoderInterface {

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static $format = 'form';

  /**
   * {@inheritdoc}
   */
  public function decode($data, $format, array $context = []) {
    $decoded = [];
    parse_str($data, $decoded);
    return $decoded;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDecoding($format) {
    return $format == static::$format;
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data, $format, array $context = []) {
    return http_build_query($data);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format) {
    return $format == static::$format;
  }

}
